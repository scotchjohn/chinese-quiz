<?php

namespace App\DataFixtures;

use App\Entity\Character;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $characterArray = self::getSampleCharacters();
        foreach ($characterArray as $characterData) {
            list($traditional, $simplified, $zhuyin, $pinyin, $definition) = $characterData;
            $character = new Character();
            $character->setTraditional($traditional);
            $character->setSimplified($simplified);
            $character->setZhuyin($zhuyin);
            $character->setPinyin($pinyin);
            $character->setDefinition($definition);
            $manager->persist($character);
        }
        $manager->flush();
    }

    /**
     * @return array
     */
    private static function getSampleCharacters(): array
    {
        return [
            ['馬', '马', 'ㄇㄚˇ', 'ma3', 'Horse'],
            ['狗', '狗', 'ㄍㄡˇ', 'gou3', 'Dog'],
            ['好', '好', 'ㄏㄠˇ', 'hao3', 'Good'],
            ['日', '日', 'ㄖˋ', 'ri4', 'Sun'],
            ['花', '花', 'ㄏㄨㄚ', 'hua1', 'Flower'],
            ['綠', '绿', 'ㄌㄩˋ', 'lv4', 'Green'],
            ['女', '女', 'ㄋㄩˇ', 'nv3', 'Woman'],
            ['月', '月', 'ㄩㄝˋ', 'yue4', 'Moon'],
            ['肉', '肉', 'ㄖㄡˋ', 'rou4', 'Meat'],
            ['草', '草', 'ㄘㄠˇ', 'cao3', 'Grass'],
            ['耳', '耳', 'ㄦˇ', 'er3', 'Nose'],
            ['紅', '红', 'ㄏㄨㄥˊ', 'hong2', 'Red'],
            ['豬', '猪', 'ㄓㄨ', 'zhu1', 'Pig'],
            ['口', '口', 'ㄎㄡˇ', 'kou3', 'Mouth'],
            ['書', '书', 'ㄕㄨ', 'shu1', 'Book']
        ];
    }
}
