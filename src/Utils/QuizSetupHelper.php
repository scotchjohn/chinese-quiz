<?php

namespace App\Utils;

use App\Entity\Character;
use App\Entity\Option;
use App\Model\QuizConfiguration;
use Doctrine\Common\Collections\ArrayCollection;

class QuizSetupHelper
{
    private const NUMBER_OF_OPTIONS = 4;

    /**
     * @param Character $character
     * @param QuizConfiguration $configuration
     * @return string
     */
    public function constructQuestionSentence(
        Character $character,
        QuizConfiguration $configuration
    ): string {
        $questionType = $configuration->getQuestionType();
        $answerType = $configuration->getAnswerType();
        if (QuizConfiguration::TYPE_CHARACTER === $questionType) {
            $localisedCharacter = $configuration->getCharacterType() === QuizConfiguration::CHARACTER_TYPE_TRADITIONAL
                ? $character->getTraditional()
                : $character->getSimplified();
            if (QuizConfiguration::TYPE_PRONUNCIATION === $answerType) {
                $sentence = 'How is '.$localisedCharacter.' pronounced?';
            } else {
                $sentence = 'What is the definition of '.$localisedCharacter.'?';
            }
        } elseif (QuizConfiguration::TYPE_PRONUNCIATION === $questionType) {
            $localisedPronunciation = $configuration->getCharacterType() === QuizConfiguration::CHARACTER_TYPE_TRADITIONAL
                ? $character->getZhuyin()
                : $character->getPinyin();
            if (QuizConfiguration::TYPE_CHARACTER === $answerType) {
                $sentence = 'Which character is pronounced '.$localisedPronunciation.'?';
            } else {
                $sentence = 'What is the definition of the character which is pronounce '.$localisedPronunciation.'?';
            }
        } else {
            if (QuizConfiguration::TYPE_CHARACTER === $answerType) {
                $sentence = 'Which character has the following definition: '.$character->getDefinition().'?';
            } else {
                $sentence = 'What is the pronunciation of the character which is defined as: '.$character->getDefinition().'?';
            }
        }
        return $sentence;
    }

    /**
     * @param Character $correctCharacter
     * @param QuizConfiguration $configuration
     * @param array $characterSet
     * @return ArrayCollection
     */
    public function getAnswerOptions(
        Character $correctCharacter,
        QuizConfiguration $configuration,
        array $characterSet
    ): ArrayCollection {
        // Create options and add the correct answer
        $options = [];
        $options[] = $this->getOptionFromConfiguration($correctCharacter, $configuration);
        shuffle($characterSet);
        foreach ($characterSet as $optionCharacter) {
            if ($optionCharacter !== $correctCharacter) {
                $options[] = $this->getOptionFromConfiguration($optionCharacter, $configuration);
                if (count($options) >= self::NUMBER_OF_OPTIONS) {
                    break;
                }
            }
        }
        shuffle($options);
        return new ArrayCollection($options);
    }

    /**
     * @param Character $character
     * @param QuizConfiguration $configuration
     * @return Option
     */
    public function getOptionFromConfiguration(
        Character $character,
        QuizConfiguration $configuration
    ): Option {
        $optionText = $this->getCharacterAnswerFromConfiguration($character, $configuration);
        $option = new Option();
        $option->setOption($optionText);
        return $option;
    }

    /**
     * @param Character $character
     * @param QuizConfiguration $configuration
     * @return string
     */
    public function getCharacterAnswerFromConfiguration(
        Character $character,
        QuizConfiguration $configuration
    ): string {
        if ($configuration->getAnswerType() === QuizConfiguration::TYPE_CHARACTER) {
            return $configuration->getCharacterType() === QuizConfiguration::CHARACTER_TYPE_TRADITIONAL
                ? $character->getTraditional()
                : $character->getSimplified();
        } elseif ($configuration->getAnswerType() === QuizConfiguration::TYPE_PRONUNCIATION) {
            return $configuration->getCharacterType() === QuizConfiguration::CHARACTER_TYPE_TRADITIONAL
                ? $character->getZhuyin()
                : $character->getPinyin();
        } else {
            return $character->getDefinition();
        }

    }
}
