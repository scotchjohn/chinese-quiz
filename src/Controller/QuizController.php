<?php

namespace App\Controller;

use App\Entity\Quiz;
use App\Factory\QuizFactory;
use App\Form\QuizConfigurationType;
use App\Form\QuizType;
use App\Model\QuizConfiguration;
use App\Repository\QuizRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class QuizController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function home(Request $request, QuizFactory $quizFactory)
    {
        // Build the quiz configuration form
        $quizConfiguration = new QuizConfiguration();
        $form = $this->createForm(QuizConfigurationType::class, $quizConfiguration);

        // Check for a valid form submission
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // Create the quiz and redirect to the quiz itself if all good
            $quiz = $quizFactory->createFromConfiguration($quizConfiguration);
            if ($quiz) {
                return $this->redirectToRoute('app_quiz', ['id' => $quiz->getId()]);
            }
        }

        return $this->render('quiz/home.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/quiz/{id}", name="app_quiz")
     */
    public function quiz(Request $request, string $id, QuizRepository $quizRepository)
    {
        /** @var Quiz $quiz */
        $quiz = $quizRepository->find($id);
        if (!$quiz) {
            throw new NotFoundHttpException("Quiz not found");
        }

        // Build the quiz form
        $form = $this->createForm(QuizType::class, $quiz);

        // Check for a valid form submission
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

        }

        return $this->render('quiz/quiz.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
