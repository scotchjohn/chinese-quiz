<?php

namespace App\Form;

use App\Model\QuizConfiguration;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuizConfigurationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('characterType', ChoiceType::class, [
                'placeholder' => ' -- Select -- ',
                'choices' => QuizConfiguration::getAllowedCharacterTypes(),
                'choice_label' => function ($choiceValue, $key, $value) {
                    return ucwords($value);
                }
            ])
            ->add('questionType', ChoiceType::class, [
                'placeholder' => ' -- Select -- ',
                'label' => 'Show me a ...',
                'choices' => QuizConfiguration::getAllowedQuestionAndAnswerTypes(),
                'choice_label' => function ($choiceValue, $key, $value) {
                    return ucwords($value);
                }
            ])
            ->add('answerType', ChoiceType::class, [
                'placeholder' => ' -- Select -- ',
                'label' => 'Ask me to give its ...',
                'choices' => QuizConfiguration::getAllowedQuestionAndAnswerTypes(),
                'choice_label' => function ($choiceValue, $key, $value) {
                    return ucwords($value);
                }
            ])
            ->add('numberOfQuestions', ChoiceType::class, [
                'placeholder' => ' -- Select -- ',
                'choices' => QuizConfiguration::ALLOWED_NUMBER_OF_QUESTIONS,
                'choice_label' => function ($choiceValue, $key, $value) {
                    return $value;
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => QuizConfiguration::class,
        ));
    }
}
