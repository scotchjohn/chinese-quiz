<?php

namespace App\Form;

use App\Entity\Option;
use App\Entity\Question;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuestionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                $form = $event->getForm();
                /** @var Question $data */
                $question = $event->getData();
                $form->add('userAnswer', EntityType::class, [
                    'class' => Option::class,
                    'label' => $question->getQuestion(),
                    'expanded' => true,
                    'choices' => $question->getOptions(),
                    'choice_label' => 'option',
                    'choice_value' => 'id',
                    'mapped' => false
                ]);
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Question::class,
            'question' => null
        ));
    }
}
