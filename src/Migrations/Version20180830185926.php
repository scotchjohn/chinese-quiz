<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180830185926 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE characters (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, traditional VARCHAR(1) NOT NULL, simplified VARCHAR(1) NOT NULL, zhuyin VARCHAR(16) NOT NULL, pinyin VARCHAR(16) NOT NULL, definition VARCHAR(1000) NOT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3A29410ECB28DBA9 ON characters (traditional)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3A29410E49B8BC9D ON characters (simplified)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE characters');
    }
}
