<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180831193437 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_8ADC54D5A412FA92');
        $this->addSql('CREATE TEMPORARY TABLE __temp__questions AS SELECT id, quiz FROM questions');
        $this->addSql('DROP TABLE questions');
        $this->addSql('CREATE TABLE questions (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, quiz INTEGER NOT NULL, question VARCHAR(1000) NOT NULL, CONSTRAINT FK_8ADC54D5A412FA92 FOREIGN KEY (quiz) REFERENCES quizzes (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO questions (id, quiz) SELECT id, quiz FROM __temp__questions');
        $this->addSql('DROP TABLE __temp__questions');
        $this->addSql('CREATE INDEX IDX_8ADC54D5A412FA92 ON questions (quiz)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_8ADC54D5A412FA92');
        $this->addSql('CREATE TEMPORARY TABLE __temp__questions AS SELECT id, quiz FROM questions');
        $this->addSql('DROP TABLE questions');
        $this->addSql('CREATE TABLE questions (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, quiz INTEGER NOT NULL)');
        $this->addSql('INSERT INTO questions (id, quiz) SELECT id, quiz FROM __temp__questions');
        $this->addSql('DROP TABLE __temp__questions');
        $this->addSql('CREATE INDEX IDX_8ADC54D5A412FA92 ON questions (quiz)');
    }
}
