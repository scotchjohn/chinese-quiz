<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180902072416 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_8ADC54D5853CD175');
        $this->addSql('CREATE TEMPORARY TABLE __temp__questions AS SELECT id, quiz_id, question, correct_answer FROM questions');
        $this->addSql('DROP TABLE questions');
        $this->addSql('CREATE TABLE questions (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, quiz_id INTEGER DEFAULT NULL, question VARCHAR(1000) NOT NULL COLLATE BINARY, correct_answer VARCHAR(1000) NOT NULL COLLATE BINARY, CONSTRAINT FK_8ADC54D5853CD175 FOREIGN KEY (quiz_id) REFERENCES quizzes (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO questions (id, quiz_id, question, correct_answer) SELECT id, quiz_id, question, correct_answer FROM __temp__questions');
        $this->addSql('DROP TABLE __temp__questions');
        $this->addSql('CREATE INDEX IDX_8ADC54D5853CD175 ON questions (quiz_id)');
        $this->addSql('DROP INDEX IDX_D035FA871E27F6BF');
        $this->addSql('CREATE TEMPORARY TABLE __temp__options AS SELECT id, question_id, option FROM options');
        $this->addSql('DROP TABLE options');
        $this->addSql('CREATE TABLE options (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, question_id INTEGER DEFAULT NULL, option VARCHAR(1000) NOT NULL COLLATE BINARY, CONSTRAINT FK_D035FA871E27F6BF FOREIGN KEY (question_id) REFERENCES questions (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO options (id, question_id, option) SELECT id, question_id, option FROM __temp__options');
        $this->addSql('DROP TABLE __temp__options');
        $this->addSql('CREATE INDEX IDX_D035FA871E27F6BF ON options (question_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_D035FA871E27F6BF');
        $this->addSql('CREATE TEMPORARY TABLE __temp__options AS SELECT id, question_id, option FROM options');
        $this->addSql('DROP TABLE options');
        $this->addSql('CREATE TABLE options (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, question_id INTEGER DEFAULT NULL, option VARCHAR(1000) NOT NULL)');
        $this->addSql('INSERT INTO options (id, question_id, option) SELECT id, question_id, option FROM __temp__options');
        $this->addSql('DROP TABLE __temp__options');
        $this->addSql('CREATE INDEX IDX_D035FA871E27F6BF ON options (question_id)');
        $this->addSql('DROP INDEX IDX_8ADC54D5853CD175');
        $this->addSql('CREATE TEMPORARY TABLE __temp__questions AS SELECT id, quiz_id, question, correct_answer FROM questions');
        $this->addSql('DROP TABLE questions');
        $this->addSql('CREATE TABLE questions (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, quiz_id INTEGER DEFAULT NULL, question VARCHAR(1000) NOT NULL, correct_answer VARCHAR(1000) NOT NULL)');
        $this->addSql('INSERT INTO questions (id, quiz_id, question, correct_answer) SELECT id, quiz_id, question, correct_answer FROM __temp__questions');
        $this->addSql('DROP TABLE __temp__questions');
        $this->addSql('CREATE INDEX IDX_8ADC54D5853CD175 ON questions (quiz_id)');
    }
}
