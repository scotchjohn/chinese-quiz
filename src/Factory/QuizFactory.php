<?php

namespace App\Factory;

use App\Entity\Character;
use App\Entity\Option;
use App\Entity\Question;
use App\Entity\Quiz;
use App\Model\QuizConfiguration;
use App\Utils\QuizSetupHelper;
use Doctrine\ORM\EntityManagerInterface;

class QuizFactory
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var QuizSetupHelper
     */
    private $quizSetupHelper;

    /**
     * QuizFactory constructor.
     * @param EntityManagerInterface $entityManager
     * @param QuizSetupHelper $quizSetupHelper
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        QuizSetupHelper $quizSetupHelper
    ) {
        $this->entityManager = $entityManager;
        $this->quizSetupHelper = $quizSetupHelper;
    }

    /**
     * @param QuizConfiguration $configuration
     * @return Quiz
     */
    public function createFromConfiguration(QuizConfiguration $configuration): Quiz
    {
        $quiz = new Quiz();

        $characterSet = $this->entityManager
            ->getRepository(Character::class)
            ->findRandom($configuration->getNumberOfQuestions());
        foreach ($characterSet as $character) {
            $question = new Question();

            $questionSentence = $this->quizSetupHelper
                ->constructQuestionSentence($character, $configuration);
            $question->setQuestion($questionSentence);

            $options = $this->quizSetupHelper
                ->getAnswerOptions($character, $configuration, $characterSet);
            /** @var Option $option */
            foreach ($options as $option) {
                $option->setQuestion($question);
                $question->addOption($option);
            }

            $correctAnswer = $this->quizSetupHelper
                ->getCharacterAnswerFromConfiguration($character, $configuration);
            $question->setCorrectAnswer($correctAnswer);

            $question->setQuiz($quiz);
            $quiz->addQuestion($question);
        }

        $this->entityManager->persist($quiz);
        $this->entityManager->flush();

        return $quiz;
    }


}
