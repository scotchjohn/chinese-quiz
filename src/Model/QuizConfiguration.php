<?php

namespace App\Model;

use InvalidArgumentException;

class QuizConfiguration
{
    public const CHARACTER_TYPE_TRADITIONAL = 'traditional';
    public const CHARACTER_TYPE_SIMPLIFIED = 'simplified';

    public const TYPE_CHARACTER = 'character';
    public const TYPE_PRONUNCIATION = 'pronunciation';
    public const TYPE_DEFINITION = 'definition';

    public const ALLOWED_NUMBER_OF_QUESTIONS = [
        5,
        10,
        15
    ];

    /**
     * @var string
     */
    private $characterType;

    /**
     * @var string
     */
    private $questionType;

    /**
     * @var string
     */
    private $answerType;

    /**
     * @var int
     */
    private $numberOfQuestions;

    /**
     * @param string $characterType
     */
    public function setCharacterType(string $characterType): void
    {
        if (in_array($characterType, self::getAllowedCharacterTypes())) {
            $this->characterType = $characterType;
        } else {
            throw new InvalidArgumentException("Invalid character type");
        }
    }

    /**
     * @return string
     */
    public function getCharacterType(): ?string
    {
        return $this->characterType;
    }

    /**
     * @param string $questionType
     */
    public function setQuestionType(string $questionType): void
    {
        if (in_array($questionType, self::getAllowedQuestionAndAnswerTypes())) {
            $this->questionType = $questionType;
        } else {
            throw new InvalidArgumentException("Invalid question type");
        }
    }

    /**
     * @return string
     */
    public function getQuestionType(): ?string
    {
        return $this->questionType;
    }

    /**
     * @param string $answerType
     */
    public function setAnswerType(string $answerType): void
    {
        if (in_array($answerType, self::getAllowedQuestionAndAnswerTypes())) {
            $this->answerType = $answerType;
        } else {
            throw new InvalidArgumentException("Invalid answer type");
        }
    }

    /**
     * @return string
     */
    public function getAnswerType(): ?string
    {
        return $this->answerType;
    }

    /**
     * @param int $numberOfQuestions
     */
    public function setNumberOfQuestions(int $numberOfQuestions): void
    {
        if (in_array($numberOfQuestions, self::ALLOWED_NUMBER_OF_QUESTIONS)) {
            $this->numberOfQuestions = $numberOfQuestions;
        } else {
            throw new InvalidArgumentException("Invalid number of questions");
        }
    }

    /**
     * @return int
     */
    public function getNumberOfQuestions(): ?int
    {
        return $this->numberOfQuestions;
    }

    /**
     * @return array
     */
    public static function getAllowedCharacterTypes(): array
    {
        return [
            self::CHARACTER_TYPE_TRADITIONAL,
            self::CHARACTER_TYPE_SIMPLIFIED
        ];
    }

    /**
     * @return array
     */
    public static function getAllowedQuestionAndAnswerTypes(): array
    {
        return [
            self::TYPE_CHARACTER,
            self::TYPE_PRONUNCIATION,
            self::TYPE_DEFINITION
        ];
    }
}
