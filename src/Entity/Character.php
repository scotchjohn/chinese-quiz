<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Character entity
 *
 * @codeCoverageIgnore
 *
 * @ORM\Table(name="characters")
 * @ORM\Entity(repositoryClass="App\Repository\CharacterRepository")
 */
class Character
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\Type("string")
     * @Assert\Length(
     *     max = 1
     * )
     * @Assert\NotNull()
     *
     * @ORM\Column(type="string", length=1, unique=true, nullable=false)
     */
    private $traditional;

    /**
     * @var string
     *
     * @Assert\Type("string")
     * @Assert\Length(
     *     max = 1
     * )
     * @Assert\NotNull()
     *
     * @ORM\Column(type="string", length=1, unique=true, nullable=false)
     */
    private $simplified;

    /**
     * @var string
     *
     * @Assert\Type("string")
     * @Assert\Length(
     *     max = 16
     * )
     * @Assert\NotNull()
     *
     * @ORM\Column(type="string", length=16, nullable=false)
     */
    private $zhuyin;

    /**
     * @var string
     *
     * @Assert\Type("string")
     * @Assert\Length(
     *     max = 16
     * )
     * @Assert\NotNull()
     *
     * @ORM\Column(type="string", length=16, nullable=false)
     */
    private $pinyin;

    /**
     * @var string
     *
     * @Assert\Type("string")
     * @Assert\Length(
     *     max = 1000
     * )
     * @Assert\NotNull()
     *
     * @ORM\Column(type="string", length=1000, nullable=false)
     */
    private $definition;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $traditional
     */
    public function setTraditional(string $traditional): void
    {
        $this->traditional = $traditional;
    }

    /**
     * @return string
     */
    public function getTraditional(): string
    {
        return $this->traditional;
    }

    /**
     * @param string $simplified
     */
    public function setSimplified(string $simplified): void
    {
        $this->simplified = $simplified;
    }

    /**
     * @return string
     */
    public function getSimplified(): string
    {
        return $this->simplified;
    }

    /**
     * @param string $definition
     */
    public function setDefinition(string $definition): void
    {
        $this->definition = $definition;
    }

    /**
     * @return string
     */
    public function getDefinition(): string
    {
        return $this->definition;
    }

    /**
     * @param string $zhuyin
     */
    public function setZhuyin(string $zhuyin): void
    {
        $this->zhuyin = $zhuyin;
    }

    /**
     * @return string
     */
    public function getZhuyin(): string
    {
        return $this->zhuyin;
    }

    /**
     * @param string $pinyin
     */
    public function setPinyin(string $pinyin): void
    {
        $this->pinyin = $pinyin;
    }

    /**
     * @return string
     */
    public function getPinyin(): string
    {
        return $this->pinyin;
    }
}
