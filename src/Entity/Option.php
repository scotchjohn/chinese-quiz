<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Option entity
 *
 * @codeCoverageIgnore
 *
 * @ORM\Table(name="options")
 * @ORM\Entity(repositoryClass="App\Repository\OptionRepository")
 */
class Option
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\Type("string")
     * @Assert\Length(
     *     max = 1000
     * )
     * @Assert\NotNull()
     *
     * @ORM\Column(type="string", length=1000, nullable=false)
     */
    private $option;

    /**
     * @var Question
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Question", inversedBy="options")
     */
    private $question;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $option
     */
    public function setOption(string $option): void
    {
        $this->option = $option;
    }

    /**
     * @return string
     */
    public function getOption(): string
    {
        return $this->option;
    }

    /**
     * @param Question $question
     */
    public function setQuestion(Question $question): void
    {
        $this->question = $question;
    }

    /**
     * @return Question
     */
    public function getQuestion(): Question
    {
        return $this->question;
    }
}
