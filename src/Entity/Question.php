<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Question entity
 *
 * @codeCoverageIgnore
 *
 * @ORM\Table(name="questions")
 * @ORM\Entity(repositoryClass="App\Repository\QuestionRepository")
 */
class Question
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\Type("string")
     * @Assert\Length(
     *     max = 1000
     * )
     * @Assert\NotNull()
     *
     * @ORM\Column(type="string", length=1000, nullable=false)
     */
    private $question;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\Option",
     *     mappedBy="question",
     *     cascade={"persist", "remove"},
     *     fetch="EAGER"
     * )
     */
    private $options;

    /**
     * @var string
     *
     * @Assert\Type("string")
     * @Assert\Length(
     *     max = 1000
     * )
     * @Assert\NotNull()
     *
     * @ORM\Column(type="string", length=1000, nullable=false)
     */
    private $correctAnswer;

    /**
     * @var Quiz
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Quiz", inversedBy="questions")
     */
    private $quiz;

    /**
     * Question constructor.
     */
    public function __construct()
    {
        $this->options = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $question
     */
    public function setQuestion(string $question): void
    {
        $this->question = $question;
    }

    /**
     * @return string
     */
    public function getQuestion(): string
    {
        return $this->question;
    }

    /**
     * @param Option $option
     */
    public function addOption(Option $option): void
    {
        $this->options->add($option);
    }

    /**
     * @param Option $option
     */
    public function removeOption(Option $option): void
    {
        $this->options->removeElement($option);
    }

    /**
     * @param Collection $options
     */
    public function setOptions(Collection $options): void
    {
        $this->options = $options;
    }

    /**
     * @return Collection
     */
    public function getOptions(): Collection
    {
        return $this->options;
    }

    /**
     * @param string $correctAnswer
     */
    public function setCorrectAnswer(string $correctAnswer): void
    {
        $this->correctAnswer = $correctAnswer;
    }

    /**
     * @return string
     */
    public function getCorrectAnswer(): string
    {
        return $this->correctAnswer;
    }

    /**
     * @param Quiz $quiz
     */
    public function setQuiz(Quiz $quiz): void
    {
        $this->quiz = $quiz;
    }

    /**
     * @return Quiz
     */
    public function getQuiz(): Quiz
    {
        return $this->quiz;
    }
}
