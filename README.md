What is it?
---

I'm learning Chinese and wanted a quick and fun way to test my knowledge of Chinese characters, their pronunciations and their definitions.

The quiz I'm building will support both Chinese character systems:

1. Traditional characters, used in Taiwan, Hong Kong and Malaysia. In Taiwan, the transliteration system for these Tradtional charactes is [zhuyin](https://en.wikipedia.org/wiki/Bopomofo).
2. Simplified characters, used in Mainland China. The transliteration system here is named [pinyin](https://en.wikipedia.org/wiki/Pinyin).

This quiz is still a work in progress, so some fundamental features are [still to be implemented](#markdown-header-to-do-list)!

Quick Setup
---

Create your SQLite database

    php bin/console doctrine:database:create
    php bin/console doctrine:migrations:migrate
    
Load sample Chinese characters

    php bin/console doctrine:fixtures:load

Start local web server 

    php bin/console server:run
    
Go to homepage and start a quiz!

    http://127.0.0.1:8000

Check the tests

    php bin/phpunit


To Do List
---

Must Have

* Randomise characters used for a quiz (create a custom DQL numeric function to represent MySQL's RAND).
* Process quiz submissions si user can see their score.
* Finish testing

Should Have

* Find a large character dictionary to import so users have maximum choice of characters to test themselves against.
* Improve look and feel

Could Have

* User accounts
* Save a list of characters to learn
* See history of quizzes taken

Known Issues / Improvements

* Refactor quiz setup services
* Storing every quiz in DB doesn't seem necessary or efficient. Could they be stored for just a limited time instead using e.g. Redis? Or persist to e.g. elasticsearch for later analysis.