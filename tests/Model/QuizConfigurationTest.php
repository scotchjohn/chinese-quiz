<?php

namespace App\Tests\Model;

use App\Model\QuizConfiguration;
use PHPUnit\Framework\TestCase;

/**
 * Class QuizConfigurationTest
 * @package App\Model
 * @coversDefaultClass App\Model\QuizConfiguration
 */
class QuizConfigurationTest extends TestCase
{
    /**
     * @var QuizConfigurationTest
     */
    private $sut;

    public function setUp()
    {
        parent::setUp();

        $this->sut = new QuizConfiguration();
    }

    /**
     * @covers ::setCharacterType
     * @covers ::getCharacterType
     * @covers ::getAllowedCharacterTypes
     */
    public function testSetCharacterTypeSuccess() {

        $this->sut->setCharacterType(QuizConfiguration::CHARACTER_TYPE_SIMPLIFIED);
        $actual = $this->sut->getCharacterType();
        $this->assertSame(QuizConfiguration::CHARACTER_TYPE_SIMPLIFIED, $actual);
    }

    /**
     * @covers ::setCharacterType
     * @covers ::getAllowedCharacterTypes
     * @expectedException InvalidArgumentException
     */
    public function testSetCharacterTypeException() {

        $this->sut->setCharacterType('nonsense');
    }

    /**
     * @covers ::setQuestionType
     * @covers ::getQuestionType
     * @covers ::getAllowedCharacterTypes
     */
    public function testSetQuestionTypeSuccess() {

        $this->sut->setQuestionType(QuizConfiguration::TYPE_PRONUNCIATION);
        $actual = $this->sut->getQuestionType();
        $this->assertSame(QuizConfiguration::TYPE_PRONUNCIATION, $actual);
    }

    /**
     * @covers ::setQuestionType
     * @covers ::getAllowedCharacterTypes
     * @expectedException InvalidArgumentException
     */
    public function testSetQuestionTypeException() {

        $this->sut->setQuestionType('nonsense');
    }

    /**
     * @covers ::setAnswerType
     * @covers ::getAnswerType
     * @covers ::getAllowedCharacterTypes
     */
    public function testSetAnswerTypeSuccess() {

        $this->sut->setAnswerType(QuizConfiguration::TYPE_PRONUNCIATION);
        $actual = $this->sut->getAnswerType();
        $this->assertSame(QuizConfiguration::TYPE_PRONUNCIATION, $actual);
    }

    /**
     * @covers ::setAnswerType
     * @covers ::getAllowedCharacterTypes
     * @expectedException InvalidArgumentException
     */
    public function testSetAnswerTypeException() {

        $this->sut->setAnswerType('nonsense');
    }

    /**
     * @covers ::setNumberOfQuestions
     * @covers ::getNumberOfQuestions
     */
    public function testSetNumberOfQuestionsSuccess() {

        $this->sut->setNumberOfQuestions(10);
        $actual = $this->sut->getNumberOfQuestions();
        $this->assertSame(10, $actual);
    }

    /**
     * @covers ::setNumberOfQuestions
     * @expectedException InvalidArgumentException
     */
    public function testSetNumberOfQuestionsException() {

        $this->sut->setNumberOfQuestions(500);
    }
}
