<?php

namespace App\Tests\Utils;

use App\Entity\Character;
use App\Model\QuizConfiguration;
use App\Utils\QuizSetupHelper;
use PHPUnit\Framework\TestCase;

/**
 * Class QuizSetupHelperTest
 * @package App\Utils
 * @coversDefaultClass App\Utils\QuizSetupHelper
 */
class QuizSetupHelperTest extends TestCase
{
    /**
     * @var QuizSetupHelperTest
     */
    private $sut;

    public function setUp()
    {
        parent::setUp();

        $this->sut = new QuizSetupHelper();
    }

    /**
     * @return array
     */
    public function providerConstructQuestionSentence(): array
    {
        $character1 = $this->getMockBuilder(Character::class)
            ->setMethods([
                'getTraditional',
                'getSimplified',
                'getZhuyin',
                'getPinyin',
                'getDefinition',
            ])
            ->getMock();
        $character1->method('getTraditional')->willReturn('馬');
        $character1->method('getSimplified')->willReturn('马');
        $character1->method('getZhuyin')->willReturn('ㄇㄚˇ');
        $character1->method('getPinyin')->willReturn('ma3');
        $character1->method('getDefinition')->willReturn('Horse');

        return [
            [
                $character1,
                QuizConfiguration::TYPE_CHARACTER,
                QuizConfiguration::TYPE_PRONUNCIATION,
                QuizConfiguration::CHARACTER_TYPE_TRADITIONAL,
                'How is 馬 pronounced?'
            ],
            [
                $character1,
                QuizConfiguration::TYPE_CHARACTER,
                QuizConfiguration::TYPE_PRONUNCIATION,
                QuizConfiguration::CHARACTER_TYPE_SIMPLIFIED,
                'How is 马 pronounced?'
            ],
        ];
    }

    /**
     * @covers ::constructQuestionSentence
     * @dataProvider providerConstructQuestionSentence
     * @param Character $character
     * @param string $questionType
     * @param string $answerType
     * @param string $characterType
     * @param string $expected
     */
    public function testConstructQuestionSentence(
        Character $character,
        string $questionType,
        string $answerType,
        string $characterType,
        string $expected
    ) {

        $configuration = $this->getMockBuilder(QuizConfiguration::class)
            ->setMethods([
                'getQuestionType',
                'getAnswerType',
                'getCharacterType',
            ])
            ->getMock();
        $configuration->method('getQuestionType')->willReturn($questionType);
        $configuration->method('getAnswerType')->willReturn($answerType);
        $configuration->method('getCharacterType')->willReturn($characterType);

        $actual = $this->sut->constructQuestionSentence($character, $configuration);

        $this->assertSame($expected, $actual);
    }
}
