<?php

namespace App\Tests\Form;

use App\Form\QuizConfigurationType;
use App\Model\QuizConfiguration;
use Symfony\Component\Form\Test\TypeTestCase;

/**
 * Class QuizConfigurationTypeTest
 * @package App\Tests\Form
 * @coversDefaultClass App\Form\QuizConfigurationType
 */
class QuizConfigurationTypeTest extends TypeTestCase
{
    /**
     * @covers ::buildForm
     * @covers ::configureOptions
     */
    public function testSubmitValidData()
    {
        $formData = array(
            'characterType' => QuizConfiguration::CHARACTER_TYPE_TRADITIONAL,
            'questionType' => QuizConfiguration::TYPE_CHARACTER,
            'answerType' => QuizConfiguration::TYPE_PRONUNCIATION,
            'numberOfQuestions' => 5,
        );

        $objectToCompare = new QuizConfiguration();
        // $objectToCompare will retrieve data from the form submission; pass it as the second argument
        $form = $this->factory->create(QuizConfigurationType::class, $objectToCompare);

        $object = new QuizConfiguration();
        $object->setCharacterType($formData['characterType']);
        $object->setQuestionType($formData['questionType']);
        $object->setAnswerType($formData['answerType']);
        $object->setNumberOfQuestions($formData['numberOfQuestions']);

        // submit the data to the form directly
        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());

        // check that $objectToCompare was modified as expected when the form was submitted
        $this->assertEquals($object, $objectToCompare);

        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }
}
